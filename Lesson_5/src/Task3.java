import java.util.Arrays;
import java.util.Scanner;

/*
3.Arraysin binarysearch methodundan istifade ederek
axtardiginiz deyerin hansi indexed yerleshdiyini tapin.
*/
public class Task3 {
    public static void main(String[] args) {
        int[] arr = {5, 7, 1, 4, 6};
        Scanner input = new Scanner(System.in);
        System.out.println("Axtardiginiz deyeri yazin: ");
        int searchedValue = input.nextInt();
        int index = Arrays.binarySearch(arr, searchedValue);
        if (index == -6) {
            System.out.println("Daxil etdiyiniz " + searchedValue + " deyeri tapilmadi");
        } else {
            System.out.println("Axtarilan deyer: " + index);
        }
    }
}
