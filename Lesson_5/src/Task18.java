import java.util.Arrays;

//18)Write a Java program to find the duplicate values
// of an array of integer values.
public class Task18 {
    public static void main(String[] args) {
        int []arr= {1,8,1,2,9,6,4,1,2};
        System.out.println("duplicate values of an array: ");
        for (int i = 0; i <arr.length-1; i++) {
            for (int j = i+1; j < arr.length; j++) {
                if(arr[i]==arr[j]){
                    System.out.println(arr[j]);

                }
            }
        }
    }
}
