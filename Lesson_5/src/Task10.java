import java.util.Arrays;

/*
10.Daxil edilmiş 2 ölçülü massivin sağ diaqonal elementlərini tərsinə çevirən proqram tərtib edin.
*/
public class Task10 {
    public static void main(String[] args) {
        int[][] massiv = {{1,2,3}, {4,5,6}, {7,8,9}};
        int[] newArr = new int[massiv.length];

        for (int i = 0; i < massiv.length ; i++) {
            for (int j = 0; j < massiv[i].length; j++) {
                    newArr[i] = massiv[i][massiv.length-i-1];
            }
        }
        System.out.println(Arrays.toString(newArr));
        int[] reversedArr = new int[newArr.length];
        int n= newArr.length;
        for (int i = 0; i <n ; i++) {
           reversedArr[i] =newArr[n-1-i];
        }
        System.out.println(Arrays.toString(reversedArr));
    }
}
