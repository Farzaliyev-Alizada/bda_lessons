//2.Daxil edilmish massivin sag ve sol diagonal
// elementlerinin cemini tapin
public class Task2 {
    public static void main(String[] args) {
        int[][] massiv = {{1,2,3}, {4,5,6}, {7,8,9}};
        int sum =0;
        for (int i = 0; i < massiv.length ; i++) {
            sum+= massiv[i][i];
            sum+=massiv[i][massiv.length-i-1];
            System.out.println(sum);
        }
        if (massiv.length%2==1){
            int middle =massiv.length/2;
            sum-=massiv[middle][middle];
        }
        System.out.println("sag ve sol diagonal elementlerinin cemi " + sum);
    }
}
