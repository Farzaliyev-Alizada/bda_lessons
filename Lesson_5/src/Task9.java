/*
9.Daxil edilmiş 2 ölçülü massivin sol diaqonal elementlərini tərsinə çevirən proqram tərtib edin.
*/
public class Task9 {
    public static void main(String[] args) {
        int[][] massiv = {{1,2,3}, {4,5,6}, {7,8,9}};
        int sum =0;
        for (int i = 0; i < massiv.length ; i++) {
            sum+= massiv[i][i];
        }

        System.out.println("sol diagonal elementlerinin cemi " + sum);
    }

}
