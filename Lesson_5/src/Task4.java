/*
4.Demek, kichik bir axtarish sistemi quracayiq. String tipinde olan massivde bir chox adlar yer alsin. Ve daha sonra consoleda bizden input istenilsin. Meselen:
        String[] users = {“Abbas”, “Leman”, “Xedice”, “Ilyas”, “Nurlan”, “Nihat”, “Elchin”, “Murad”, “Mirhesen”, “Emin”, “Farid”, “Terane”}; - deye bir massivimiz var.
        Men consoledan ‘tera’ – daxil ederken proqram anlasin ki, men teraneni axtariram. Ve hemin adi tamamlayaraq yaninda indexide olmaqla chap etsin. Yox eger uygun gelen bir chox ad varsa hamisini  chap etsin. Eks halda ise bele bir user yoxdur desin
*/

import java.util.Arrays;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        String users[] = {"Abbas", "Nicat", "Xedice", "Ilyas", "Nurlan", "Nihat", "Elchin", "Murad", "Mirhesen", "Emin", "Farid", "Terane"};
        Scanner input = new Scanner(System.in);
        System.out.println("Istifadeci adini daxil edin: ");
        String user = input.nextLine();
        SearchUser(users, user);
    }

    public static void SearchUser(String[] arr, String input) {
        boolean found = false;
        for (int i = 0; i < arr.length; i++) {

            if (arr[i].toLowerCase().contains(input.toLowerCase())) {
                System.out.println(arr[i] + " - index:" + i);
                found = true;
            } }
        if(!found){
                System.out.println("User tapilmadi");
            }


        }
}