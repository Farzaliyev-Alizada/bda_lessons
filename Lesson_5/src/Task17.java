//17)Write a Java program to reverse an array of integer values.
import java.util.Arrays;

public class Task17 {
    public static void main(String[] args) {
        int[] massiv = {1,2,3,4,5,6,7,8,9};
        int[] reversedArr = new int[massiv.length];
        int n= massiv.length;
        for (int i = 0; i <n ; i++) {
            reversedArr[i] =massiv[n-1-i];
        }
        System.out.println(Arrays.toString(reversedArr));
    }
}
