import java.util.Arrays;

/*
6.Daxil edilmiş 2 ölçülü massivin bütün elementlərinin cəmini hesablayan proqram yazın.
*/
public class Task6 {
    public static void main(String[] args) {

        int[][] massiv = {{1, 10, 3}, {4, 5, 20}};
        int sum = 0;
        for (int i = 0; i < massiv.length; i++) {
            for (int j = 0; j < massiv[i].length; j++) {
                int number = massiv[i][j];
                sum += number;
            }
        }
        System.out.println(sum);
    }
}
