/*
7.Daxil edilən 2 ölçülü kvadrat matrisin baş diaqonal elementlərini çap edən proqram yazın. (int[][], for, şərt)
*/
public class Task7 {
    public static void main(String[] args) {
        int[][] massiv = {{1,2,3,5}, {4,5,6,5}, {7,8,9,5},{10,11,12,5}};
        int sum =0;
        for (int i = 0; i < massiv.length ; i++) {
            sum+= massiv[i][i];
            sum+=massiv[i][massiv.length-i-1];

        }
        if (massiv.length%2==1){
            int middle =massiv.length/2;
            sum-=massiv[middle][middle];
        }
        System.out.println("baş diagonal elementlerinin cemi " + sum);
    }
}
