import java.util.Arrays;
import java.util.Scanner;

/*
15)Write a Java program to find the index of an array element.
*/
public class Task15 {
    public static void main(String[] args) {
        int[] arr = {5, 7, 1, 4, 6};
        Scanner input = new Scanner(System.in);
        System.out.println("Axtardiginiz deyeri yazin: ");
        int searchedValue = input.nextInt();
        int index = Arrays.binarySearch(arr, searchedValue);
        if (index == -6) {
            System.out.println("Daxil etdiyiniz " + searchedValue + " deyeri tapilmadi");
        } else {
            System.out.println("Axtarilan deyer: " + index);
        }
    }
}
