/*11.Write a Java program to sum values of an array.*/
public class Task11 {
    public static void main(String[] args) {

        int[][] massiv = {{1, 10, 3}, {4, 5, 20}};
        int sum = 0;
        for (int i = 0; i < massiv.length; i++) {
            for (int j = 0; j < massiv[i].length; j++) {
                int number = massiv[i][j];
                sum += number;
            }
        }
        System.out.println(sum);
    }
}
