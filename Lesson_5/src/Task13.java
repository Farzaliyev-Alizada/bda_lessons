//13)Write a Java program to calculate the average value of array elements.
public class Task13 {
    public static void main(String[] args) {

        int[] massiv = {2, 10, 3};
        int sum = 0;
        int  length= massiv.length;
        for (int i = 0; i < massiv.length; i++) {
                int number = massiv[i];
                sum += number;
        }
        double average= sum/length;
        System.out.println(average);
    }
}
