/*
8.Daxil edilmiş 2 ölçülü massivin sağ diaqonal elementlərini çap edən proqram yazın.
*/
public class Task8 {
    public static void main(String[] args) {
        int[][] massiv = {{1,2,3}, {4,5,6}, {7,8,9}};
        int sum =0;
        for (int i = 0; i < massiv.length ; i++) {
            sum+=massiv[i][massiv.length-i-1];

        }
        System.out.println("sag diagonal elementlerinin cemi " + sum);
    }

}
