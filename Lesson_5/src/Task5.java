import java.util.Arrays;

//5.Daxil edilmiş 2 ölçülü massivin bütün 5-ə bölünən ədədlərini çap edən proqram tərtib edin.
public class Task5 {
    public static void main(String[] args) {

        int[][] massiv = {{1,10,3}, {4,5,20}};
        int[] newArr = new int[massiv.length*massiv[0].length];
        int index=0;
        for (int i = 0; i < massiv.length ; i++) {
            for (int j = 0; j < massiv[i].length; j++) {
                if (massiv[i][j] % 5 == 0) {
                    newArr[index] = massiv[i][j];
                    index++;
                }
            }
        }
            System.out.println(Arrays.toString(newArr));

    }
}
