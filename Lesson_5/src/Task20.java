import java.util.Arrays;
import java.util.Scanner;

//20)scanner ile int massivi doldurulur və bu
// massiv methoda ötürülür, method bu massivdeki ededleri toplayir
// ve geriye qaytarir. Meselen: {1,2,3,4,5} method return edecek 1+2+3+4+5;
public class Task20 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n=5;
        int sum =0;
        int[] arr= new int [n];
        System.out.println(n+" sayda eded daxil edin");
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }
        System.out.println(Arrays.toString(arr));
        for (int i = 0; i < arr.length; i++) {
          sum+=arr[i];
        }
        System.out.println(sum);
    }
}
