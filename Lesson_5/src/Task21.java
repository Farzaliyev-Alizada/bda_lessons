import java.util.Arrays;
import java.util.Scanner;

//21). scanner ile int massivi doldurulur və bu massiv methoda
// ötürülür, eger massivdəki reqemlerden // her hansisa 2-si bir
// birine beraberdirse geriye true qaytarsin
public class Task21 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n=5;
        boolean isDulicate=false;
        int[] arr= new int [n];
        System.out.println(n+" sayda eded daxil edin");
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }
        System.out.println(Arrays.toString(arr));
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = i+1; j < arr.length; j++) {
             if(arr[i]==arr[j]) {
                 isDulicate=true;
             }
            }

        }
        System.out.println(isDulicate);
    }
}
