import java.util.Scanner;

/*
14)Write a Java program to test if an array contains a specific value.
*/
public class Task14 {
    public static void main(String[] args) {
        String users[] = {"Abbas", "Nicat", "Xedice", "Ilyas", "Terane"};
        Scanner input = new Scanner(System.in);
        System.out.println("Istifadeci adini daxil edin: ");
        String user = input.nextLine();
        SearchUser(users, user);
    }

    public static void SearchUser(String[] arr, String input) {
        boolean found = false;
        for (int i = 0; i < arr.length; i++) {

            if (arr[i].toLowerCase().contains(input.toLowerCase())) {
                System.out.println(arr[i] + " - index:" + i);
                found = true;
            } }
        if(!found){
            System.out.println("User tapilmadi");
        }


    }
}
