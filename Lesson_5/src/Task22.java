import java.util.Arrays;
import java.util.Scanner;

//22). scanner ile int massivi doldurulur və bu massiv methoda
// oturulur eger ededler artan ardicilliqla daxil edilibse
// geriye true qaytarsin eks halda false
public class Task22 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int n = 5;

        int[] arr = new int[n];
        System.out.println(n + " sayda eded daxil edin");
        for (int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }
        System.out.println(doIt(arr));
    }

    public static boolean doIt(int[] arr) {
        boolean isDulicate = false;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] < arr[j]) {
                    isDulicate = true;
                } else{
                    isDulicate=false;
                }
            }

        }

        return isDulicate;
    }

}
