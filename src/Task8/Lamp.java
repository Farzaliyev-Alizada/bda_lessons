package Task8;

public class Lamp {
    private boolean isOn;

    public Lamp() {
        this.isOn = false;
    }

    public boolean isOn() {
        return this.isOn;
    }

    public void turnOn() {
        this.isOn = true;
    }

    public void turnOff() {
        this.isOn = false;
    }

    public void printState() {
        if (isOn()) {
            System.out.println("Lamp is on.");
        } else {
            System.out.println("Lamp is off.");
        }
    }
}
