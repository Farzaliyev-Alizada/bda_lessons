package Task8;

public class Main {
    public static void main(String[] args) {
        Lamp lamp = new Lamp();
        lamp.printState();

        lamp.turnOn();
        lamp.printState();

        lamp.turnOff();
        lamp.printState();
    }
}
