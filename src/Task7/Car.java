package Task7;

public class Car {
    private static int numberOfCars = 0;//static field
    private String make;// instance field
    private String model;// instance field
    private int year;// instance field

    public Car(String make, String model, int year) {
        this.make = make;
        this.model = model;
        this.year = year;
        numberOfCars++;
    }

    public static int getNumberOfCars() {  //static method
        return numberOfCars;
    }

    public String getMake() { //instance methodlar
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {  //instance methodlar
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() { //instance methodlar
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
