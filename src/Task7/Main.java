package Task7;

public class Main {
    public static void main(String[] args) {
        // Static field və methodlara misal
        System.out.println("Number of cars: " + Car.getNumberOfCars());

        // Instance field və methodlara misal
        Car car1 = new Car("Toyota", "Corolla", 2021);
        System.out.println("Make of car 1: " + car1.getMake());
        System.out.println("Model of car 1: " + car1.getModel());
        System.out.println("Year of car 1: " + car1.getYear());

        Car car2 = new Car("Honda", "Civic", 2022);
        System.out.println("Make of car 2: " + car2.getMake());
        System.out.println("Model of car 2: " + car2.getModel());
        System.out.println("Year of car 2: " + car2.getYear());

        car1.setMake("Ford");
        car1.setModel("Mustang");
        car1.setYear(2020);
        System.out.println("Make of car 1 after modification: " + car1.getMake());
        System.out.println("Model of car 1 after modification: " + car1.getModel());
        System.out.println("Year of car 1 after modification: " + car1.getYear());

        System.out.println("Number of cars after modification: " + Car.getNumberOfCars());
    }
}
