package Task12_20;

public class Student {
    private String name;
    private int grade;
    private int age;

    public Student(String name, int grade, int age) {
        this.name = name;
        this.grade = grade;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void printName() {
        System.out.println("Student's name is " + name);
    }

    public void printGrade() {
        System.out.println(name + " is in grade " + grade);
    }

    public void printAge() {
        System.out.println(name + " is " + age + " years old");
    }
}
