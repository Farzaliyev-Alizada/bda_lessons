package Task12_20;

public class Main {
    public static void main(String[] args) {

        Teacher teacher = new Teacher("Tarana Nasirova", "Back End Developer", 25);


        System.out.println("Teacher's name: " + teacher.getName());
        System.out.println("Teacher's subject: " + teacher.getSubject());
        System.out.println("Teacher's age: " + teacher.getAge());


        teacher.setName("Ali Ashrafli");
        teacher.setAge(22);


        teacher.printName();
        teacher.printSubject();
        teacher.printAge();


        Student student = new Student("Bob Marley", 9, 40);


        System.out.println("Student's name: " + student.getName());
        System.out.println("Student's grade: " + student.getGrade());
        System.out.println("Student's age: " + student.getAge());


        student.setName("Alizada Farzaliyev");
        student.setGrade(1);


        student.printName();
        student.printGrade();
        student.printAge();
    }
}
