package Task9;
public class Main {
    public static void main(String[] args) {
        Customer alizada = new Customer("Alizada", "Farzaliyev" ,5000);
        Customer bob = new Customer("Bob", "Marley",100);

        alizada.sendMoney(bob, 1000);

        System.out.println("Bob's balance: " + bob.getBalance());
    }
}
