package Task9;

public class Customer {
    private String name;
    private String surname;
    private String email;
    private double balance;


    public Customer(String name, String surname, double balance) {
        this.name = name;
        this.surname = surname;
        this.balance = balance;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void receiveMoney(Customer recipient, double amount) {
        balance += amount;
        System.out.println(amount + " AZN received."+ recipient.getName() +" current balance: " + balance + " AZN.");
    }


    public void sendMoney(Customer recipient, double amount) {
        if (balance >= amount) {
            balance -= amount;
            recipient.receiveMoney(recipient,amount);
            System.out.println(amount + " AZN sent to " + recipient.getName() + ". Current balance: " + balance + " AZN.");
        } else {
            System.out.println("Insufficient balance. Transaction failed.");
        }
    }
}
