package Task4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("How many users do you want to register? ");
        int n = scanner.nextInt();

        User[] users = new User[n];


        for (int i = 0; i < n; i++) {
            System.out.print("Enter user name: ");
            String name = scanner.next();
            users[i] = new User(name);
        }


        for (int i = 0; i < n; i++) {
            System.out.println("User " +i +": "+users[i].getName());
        }
    }
}
