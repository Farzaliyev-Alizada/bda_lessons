package Task5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int choice;
        int n;

        System.out.print("Neçə user qeydiyyatdan keçirmək istəyirsiz? ");
        n = scanner.nextInt();

        User[] users = new User[n];


        for (int i = 0; i < n; i++) {
            System.out.print("User adını daxil et: ");
            String name = scanner.next();
            System.out.print("User soyadını daxil et: ");
            String surname = scanner.next();
            users[i] = new User(name, surname);
        }

        while (true) {
            System.out.println("\nSeçiminizi edin:");
            System.out.println("1. Register et");
            System.out.println("2. Sistemden çıx");
            choice = scanner.nextInt();

            if (choice == 1) {
                System.out.println("\nSeçiminizi edin:");
                System.out.println("1. User axtar");
                System.out.println("2. Sistemden çıx");

                choice = scanner.nextInt();

                if (choice == 1) {
                    System.out.print("\nTapmaq istədiyiniz user adını daxil edin: ");
                    String searchName = scanner.next();

                    boolean found = false;
                    for (User user : users) {
                        if (user.getName().equals(searchName)) {
                            found = true;
                            System.out.println(user.toString());
                        }
                    }

                    if (!found) {
                        System.out.println("Belə bir user yoxdur.");
                    }
                } else if (choice == 2) {
                    break;
                }
            } else if (choice == 2) {
                break;
            }
        }
    }
}
