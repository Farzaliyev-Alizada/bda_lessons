package Task19;

public class Company {
    private String name;
    private int budget;
    private Employee[] employees;

    public Company(String name, int budget, Employee[] employees) {
        this.name = name;
        this.budget = budget;
        this.employees = employees;
    }

    public int getTotalSalary() {
        int totalSalary = 0;
        for (Employee employee : employees) {
            totalSalary += employee.getSalary();
        }
        return totalSalary;
    }

    public boolean isBudgetExceeded() {
        int totalSalary = getTotalSalary();
        return totalSalary > budget;
    }

    public String getName() {
        return name;
    }

    public int getBudget() {
        return budget;
    }

    public Employee[] getEmployees() {
        return employees;
    }
}
