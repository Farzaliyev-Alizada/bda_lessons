package Task19;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter company name:");
        String companyName = input.nextLine();

        System.out.println("Enter company budget:");
        int companyBudget = input.nextInt();

        System.out.println("Enter number of employees:");
        int numEmployees = input.nextInt();
        Employee[] employees = new Employee[numEmployees];

        for (int i = 0; i < numEmployees; i++) {
            System.out.println("Enter information for employee #" + (i + 1));
            System.out.println("Enter full name:");
            String fullName = input.nextLine();
            input.nextLine(); // to consume the new line character

            System.out.println("Enter department:");
            String department = input.nextLine();

            System.out.println("Enter salary:");
            int salary = input.nextInt();

            employees[i] = new Employee(fullName, department, salary);
        }

        Company company = new Company(companyName, companyBudget, employees);

        System.out.println("Total salary: " + company.getTotalSalary());

        if (company.isBudgetExceeded()) {
            System.out.println("Budget exceeded!");
        } else {
            System.out.println("Budget not exceeded!");
        }
    }
}
