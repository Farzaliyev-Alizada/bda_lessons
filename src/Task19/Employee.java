package Task19;

public class Employee {
    private String fullName;
    private String department;
    private double salary;

    public Employee(String fullName, String department, double salary) {
        this.fullName = fullName;
        this.department = department;
        this.salary = salary;
    }

    public void printInfo() {
        System.out.println("Name: " + fullName );
        System.out.println("Department: " + department);
        System.out.println("Salary: " + salary);
    }

    public void salaryIncrease(double amount) {
        salary += amount;
        System.out.println(fullName + "'s salary increased to " + salary);
    }

    public void salaryDecrease(double amount) {
        if (salary - amount < 0) {
            System.out.println("Error: Salary can't be negative.");
            return;
        }
        salary -= amount;
        System.out.println(fullName + "'s salary decreased to " + salary);
    }

    public double getSalary() {
        return salary;
    }
}
