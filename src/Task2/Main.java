package Task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("How many users do you want to register? ");
        int numUsers = scanner.nextInt();
        scanner.nextLine();

        User[] users = new User[numUsers];

        for (int i = 0; i < numUsers; i++) {
            System.out.println("Enter User #" + (i+1) + " information:");

            System.out.print("User ID: ");
            int id = scanner.nextInt();
            scanner.nextLine();

            System.out.print("User Name: ");
            String name = scanner.nextLine();

            System.out.print("User Email: ");
            String email = scanner.nextLine();

            User user = new User();
            user.setId(id);
            user.setName(name);
            user.setEmail(email);

            users[i] = user;
        }

        System.out.println("Registered Users:");
        for (User user : users) {
            System.out.println("User ID: " + user.getId());
            System.out.println("User Name: " + user.getName());
            System.out.println("User Email: " + user.getEmail());
            System.out.println();
        }

        scanner.close();
    }
}
