package Task16_17_18;

public class Employee {
    private String fullName;

    private String department;
    private double salary;

    public Employee(String fullName, String department, double salary) {
        this.fullName = fullName;
        this.department = department;
        this.salary = salary;
    }

    public void printInfo() {
        System.out.println("Employee information:");
        System.out.println("- Name: " + fullName);
        System.out.println("- Department: " + department);

        System.out.println("- Salary: " + salary);
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }


    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public void increaseSalary(int amount) {
        this.salary += amount;
    }

    public void decreaseSalary(int amount) {
        if (this.salary - amount >= 0) {
            this.salary -= amount;
        } else {
            System.out.println("Error: Salary cannot be negative");
        }
    }
}
