package Task16_17_18;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input1 = new Scanner(System.in);
        System.out.println("How many employees do you want to add?");
        int num = input1.nextInt();

        Employee[] employees = new Employee[num];

        for (int i = 0; i < num; i++) {
            Scanner input = new Scanner(System.in);
            System.out.println("Enter full name");
            String fullName = input.nextLine();

            System.out.println("Enter department");
            String department = input.nextLine();

            System.out.println("Enter salary");
            int salary = input.nextInt();

            employees[i] = new Employee(fullName, department, salary);
            employees[i].printInfo();
        }
    }
}
