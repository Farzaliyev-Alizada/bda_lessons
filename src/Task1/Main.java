package Task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter User ID: ");
        int id = scanner.nextInt();
        scanner.nextLine();

        System.out.print("Enter User Name: ");
        String name = scanner.nextLine();

        System.out.print("Enter User Email: ");
        String email = scanner.nextLine();

        User user = new User();
        user.setId(id);
        user.setName(name);
        user.setEmail(email);

        System.out.println("User ID: " + user.getId());
        System.out.println("User Name: " + user.getName());
        System.out.println("User Email: " + user.getEmail());

        scanner.close();
    }
}

