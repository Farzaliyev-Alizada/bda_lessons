package Task13;

import java.util.Scanner;

public class Student {
    private String name;
    private int grade;
    private int age;

    public Student(String name, int grade, int age) {
        this.name = name;
        this.grade = grade;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void printName() {
        System.out.println("Student's name: " + name);
    }

    public void printGrade() {
        System.out.println("Student's grade: " + grade);
    }

    public void printAge() {
        System.out.println("Student's age: " + age);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of students: ");
        int numStudents = scanner.nextInt();

        Student[] students = new Student[numStudents];

        for (int i = 0; i < numStudents; i++) {
            System.out.println("Enter details for student #" + (i+1));
            System.out.print("Name: ");
            String name = scanner.next();
            System.out.print("Grade: ");
            int grade = scanner.nextInt();
            System.out.print("Age: ");
            int age = scanner.nextInt();

            students[i] = new Student(name, grade, age);
        }

        for (Student student : students) {
            System.out.println("Student details:");
            student.printName();
            student.printGrade();
            student.printAge();
        }
    }
}
