package Task13;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        System.out.println("Enter teacher details:");
        System.out.print("Name: ");
        String teacherName = scanner.next();
        System.out.print("Subject: ");
        String teacherSubject = scanner.next();
        System.out.print("Age: ");
        int teacherAge = scanner.nextInt();

        Teacher teacher = new Teacher(teacherName, teacherSubject, teacherAge);


        System.out.print("Enter the number of students: ");
        int numStudents = scanner.nextInt();

        Student[] students = new Student[numStudents];

        for (int i = 0; i < numStudents; i++) {
            System.out.println("Enter details for student #" + (i+1));
            System.out.print("Name: ");
            String studentName = scanner.next();
            System.out.print("Grade: ");
            int studentGrade = scanner.nextInt();
            System.out.print("Age: ");
            int studentAge = scanner.nextInt();

            students[i] = new Student(studentName, studentGrade, studentAge);
        }


        System.out.println("\nTeacher details:");
        teacher.printName();
        teacher.printSubject();
        teacher.printAge();

        System.out.println("\nStudent details:");
        for (Student student : students) {
            System.out.println("--------------------");
            student.printName();
            student.printGrade();
            student.printAge();
        }
    }
}
