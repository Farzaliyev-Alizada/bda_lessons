package Task13;

public class Teacher {
    private String name;
    private String subject;
    private int age;

    public Teacher(String name, String subject, int age) {
        this.name = name;
        this.subject = subject;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void printName() {
        System.out.println("Teacher's name: " + name);
    }

    public void printSubject() {
        System.out.println("Teacher's subject: " + subject);
    }

    public void printAge() {
        System.out.println("Teacher's age: " + age);
    }
}
