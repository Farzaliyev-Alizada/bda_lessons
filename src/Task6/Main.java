package Task6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Employee[] employees = new Employee[3];

        for (int i = 0; i < employees.length; i++) {
            employees[i] = new Employee();
            System.out.print("Employee " + (i + 1) + " Name: ");
            employees[i].setName(scanner.nextLine());
            System.out.print("Employee " + (i + 1) + " Age: ");
            employees[i].setAge(scanner.nextInt());
            scanner.nextLine();
            System.out.print("Employee " + (i + 1) + " Salary: ");
            employees[i].setSalary(scanner.nextDouble());
            scanner.nextLine();
            System.out.print("Employee " + (i + 1) + " Position: ");
            employees[i].setPosition(scanner.nextLine());
            System.out.print("Employee " + (i + 1) + " Department: ");
            employees[i].setDepartment(scanner.nextLine());
        }

        System.out.println("Employee Names:");
        for (Employee employee : employees) {
            System.out.println(employee.getName());
        }
    }
}
