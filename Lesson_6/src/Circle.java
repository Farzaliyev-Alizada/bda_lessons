import java.util.Scanner;

public class Circle {
    private final int r;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Please ender radius of the circle: ");
        int num = input.nextInt();
        Circle circ = new Circle(num);
        circ.areaOfCircle(num);
        circ.lengthOfCircle(num);
    }
    public Circle (int r) {
        this.r=r;
    }
    public void lengthOfCircle (int rad) {
        double length = 2* Math.PI* rad;
        System.out.println("the length of the circle is: " +length);
    }
    public void areaOfCircle (int rad) {
        double area =  Math.sqrt(Math.PI)* rad;
        System.out.println("the area of the circle is: " +area);
    }
}
