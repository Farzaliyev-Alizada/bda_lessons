package Book;

public class Book {
    int AllPages;
    int currentPage;
    String name;
    String author;

    public Book(int allPages, int currentPage, String name, String author) {
        AllPages = allPages;
        this.currentPage = currentPage;
        this.name = name;
        this.author = author;
    }

    public int getAllPages() {
        return AllPages;
    }

    public void setAllPages(int allPages) {
        AllPages = allPages;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
