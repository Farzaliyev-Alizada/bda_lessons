/*
4.	Area adlı bir class yaradaq. Və biz onun içində olan
bir methoda length və height göndərək. Bizə həm sahəni,
 həm də perimetri hesablayıb qaytarsın.
*/
import java.util.Scanner;

public class Area {
          private int l;
          private int w;

        public static void main(String[] args) {
            Scanner input = new Scanner(System.in);
            System.out.println("Please ender length of the rectangular: ");
            int length = input.nextInt();
            System.out.println("Please ender width of the rectangular: ");
            int width = input.nextInt();

            Area ArAndPerim = new Area(length,width);
            ArAndPerim.area(length,width);
            ArAndPerim.perimeter(length,width);
        }
        public Area (int l, int w) {
            this.l=l;
            this.w=w;
        }
        public void perimeter (int len,int wid) {
            double permtr = 2*(len+wid);
            System.out.println("the perimeter of the circle is: " +permtr);
        }
        public void area (int len,int wid) {
            double area = len*wid ;
            System.out.println("the area of the circle is: " +area);
        }
    }

