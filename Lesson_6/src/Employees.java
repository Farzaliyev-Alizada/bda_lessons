/*1.	Employee classı yaradın. Və içərisində (id, full-name,
        company, department, position və salary) olsun.
        Daha sonra bir method yaradın, məlumatları
        içərisinə parametr olaraq göndərək və bizə console
        da məlumatları çap etsin.*/
public class Employees {
    public static void main(String[] args) {
        Employees employee = new Employees
                (1,
                "Alizada Farzaliyev",
                "NASA",
                "IT",
                "Developer",
                10000);
        employee.printInformation();
    }
    private int id;
    private String fullName;
    private String company;
    private String department;
    private String position;
    private double salary;

    public Employees(int id, String fullName, String company, String department, String position, double salary){
        this.id=id;
        this.fullName=fullName;
        this.company=company;
        this.department=department;
        this.position=position;
        this.salary=salary;
    }
    public void printInformation(){
        System.out.println("ID:"+id);
        System.out.println("full Name:"+fullName);
        System.out.println("company:"+company);
        System.out.println("department:"+department);
        System.out.println("position:"+position);
        System.out.println("salary:"+salary);

    }

}