package Time;

public class Time {
    int minute;

    public Time(int minute) {
        this.minute = minute;
    }

    public int getTime() {
        return minute;
    }

    public void setTime(int minute) {
        this.minute = minute;
    }
}
