package Employee;

public class Employee {
    String name;
    String surname;
    String department;
    String position;
    int salary;

    public Employee(String name, String surname, String department, String position, int salary) {
        this.name = name;
        this.surname = surname;
        this.department = department;
        this.position = position;
        this.salary = salary;
    }

    public void getAllInfo (){
        System.out.println("name: "+ this.name+
                "\nsurname: "+ this.surname+
                "\ndepartment: "+ this.department+
                "\nposition: "+ this.position+
                "\nsalary: "+ this.salary
                );
    }
    public void IncreaseSalary () {
        salary+=500;
        System.out.println("New salary: "+salary);
    }
    public void DecreaseSalary () {
         salary-=100;
        System.out.println("New salary: "+salary);
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
