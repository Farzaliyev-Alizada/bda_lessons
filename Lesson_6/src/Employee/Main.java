package Employee;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Ad daxil edin");
        String name = input.nextLine();

        System.out.println("Soyad daxil edin");
        String surname = input.nextLine();

        System.out.println("Department daxil edin");
        String department = input.nextLine();

        System.out.println("Position daxil edin");
        String position = input.nextLine();

        Scanner input1 = new Scanner(System.in);
        System.out.println("Salary daxil edin");
        int salary = input1.nextInt();

        Employee emp = new Employee(name, surname,department,position,salary);
        emp.getAllInfo();
        emp.IncreaseSalary();
        emp.DecreaseSalary();
        emp.setPosition("alim");
        System.out.println(emp.getPosition());
    }
}
