package Date;


public class Date {
    int day;

    public Date(int day) {
        this.day = day;
    }

    public int getDay() {
        return day;
    }
    public void setDay(int day) {
        this.day = day;
    }

}
