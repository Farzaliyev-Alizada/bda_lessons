/*6.	Bir array classı yaradın. İçərisin də bir çox method olsun.
        (max elem tapsın, min elem tapsın, onları sıralasın,
        içərisinə göndərilən elementin indexini tapsın, içərisinə
        göndərilən indexə uyğun gələn elementi tapsın, içərisinə
        göndərilən indexə uyğun dəyəri silsin və s.) bunların
        hər biri ayrı-ayrılıqda Array clasının methodları olsun.*/

import java.util.Arrays;
import java.util.Scanner;

public class Array {
    public static void main(String[] args) {
        int[] arr = {5, 7, 1, 2, 6, 9};
        max(arr);
        sort(arr);
        findIndex(arr);
        removeIndex(arr);
    }

    public static void max(int[] array) {
        int max = array[0];
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
            if (array[i] < min) {
                min = array[i];
            }
        }
        System.out.println("Max number: " + max);
        System.out.println("Max number: " + min);
    }

    public static void sort(int[] array) {
        int temp = 0;
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }

    public static void findIndex(int[] array) {
        Scanner input = new Scanner(System.in);
        System.out.println("insert index to find: ");
        int index =input.nextInt();
        int result  = array[index];
        System.out.println("The given "+index+" index is "+result);
    }

    public static void removeIndex(int[] array) {
        Scanner input = new Scanner(System.in);
        System.out.println("insert index to find: ");
        int index =input.nextInt();

        for (int i = index; i < array.length-1; i++) {
            array[i]=array[i+1];
        }
        array=Arrays.copyOf(array, array.length-1);

        System.out.println(Arrays.toString(array));
    }
}
